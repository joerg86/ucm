from django.shortcuts import render
from django.views.generic import TemplateView
from map.models import Event, UserProfile
from django.contrib.gis.db.models.functions import Distance

class MapView(TemplateView):
    template_name = "map/index.html"

    def get_context_data(self, **kwargs):
        context = super(MapView, self).get_context_data()
        events = Event.objects.all()
        
        if self.request.user.is_authenticated():
            try:
                pos = self.request.user.userprofile.geopos
            except UserProfile.DoesNotExist:
                pass
            else:
                events = events.annotate(distance=Distance("geopos", pos))

        context["events"] = events
        return context

# Create your views here.
