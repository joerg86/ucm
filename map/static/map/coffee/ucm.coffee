app = angular.module "ucm", [ "ui.bootstrap", "ui-leaflet", "restangular", "angularMoment" ]

app.config (RestangularProvider)->
  RestangularProvider.setBaseUrl "/api"


app.controller "MapController", ["Restangular", "moment", "$filter", (Restangular, moment, $filter) ->
    vm = this

    vm.controls =
        custom: []

    vm.layers =
        baselayers:
            openStreetMap:
                name: 'OpenStreetMap'
                type: 'xyz'
                url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        overlays:
            events:
                type: 'group'
                name: 'Events'
                visible: true
            users:
                type: 'group'
                name: 'Users'
                visible: true

    vm.center =
        autoDiscover: true
        lat: 52
        lng: 8
        zoom: 6
       
    sidebar = L.control.sidebar('map-sidebar')
    vm.controls.custom.push sidebar

    vm.markers = {}

    vm.focusEvent = (id) ->
        for key, m of vm.markers
            do (m) ->
                m.focus = false

        vm.markers["event" + id].focus = true


    eventIcon =
        type: "awesomeMarker"
        icon: 'calendar',
        markerColor: 'red'
        prefix: 'fa'

    userIcon =
        type: "awesomeMarker"
        icon: 'user',
        markerColor: 'blue'
        prefix: 'fa'

    homeIcon =
        type: "awesomeMarker"
        icon: 'home',
        markerColor: 'cadetblue'
        prefix: 'fa'


    Event = Restangular.all("events")
    Event.getList().then (data)->
        for event in data
            do (event) ->
                thumbnail = if event.thumbnail then event.thumbnail else "http://placehold.it/100x100"
                distance = if event.distance != null then $filter("number")(event.distance, 1) + " km"  else ""

                vm.markers["event"+event.id] =
                    message:
                        """
                        <h4><i class="fa fa-calendar"></i> <a href=''>#{event.title}</a></h4>
                        <div class='pull-left' style='margin-right:10px'>
                            <a class='thumbnail'><img src='#{thumbnail}'></a>
                        </div>
                        <div>
                            <small>
                                <i class="fa fa-calendar-o fa-fw"></i> #{moment(event.begin_date).format("L LT")}<br>
                                <i class="fa fa-map-marker fa-fw"></i> #{event.city}, #{event.country} <small class="text-info">#{distance}</small>
                            </small>
                        </div>
                        <p><small>#{event.description}</small></p>
                        <div class="clearfix"></div>
                        """
                    lng: event.geopos.coordinates[0]
                    lat: event.geopos.coordinates[1]
                    icon: eventIcon
                    group: "events"
                    layer: "events"

    Profile = Restangular.all("profiles")
    Profile.getList().then (data)->
        for prof in data
            do (prof) ->
                thumbnail = if prof.thumbnail then prof.thumbnail else "http://placehold.it/100x100"
                distance = if prof.distance != null then $filter("number")(prof.distance, 1) + " km"  else ""
                icon = if prof.user.current then homeIcon else userIcon

                vm.markers["profile"+prof.id] =
                    message:
                        """
                        <div class='pull-left' style='margin-right:10px'>
                            <a class='thumbnail'><img src='#{thumbnail}'></a>
                        </div>
                        <div>
                            <h4><i class="fa fa-user"></i> <a href=''>#{prof.display_name}</a></h4>
                            <i class='fa fa-map-marker'></i> #{prof.city}, #{prof.country} <small class='text-info'>#{distance}</small>
                            <p>
                                <small>
                                    #{prof.description}    
                                </small>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class='pull-right btn-group btn-group-sm'>
                            <button class='btn btn-default'><i class='fa fa-user'></i></button>
                            <button class='btn btn-primary'><i class='fa fa-envelope'></i></button>
                            <button class='btn btn-success'><i class='fa fa-star'></i>
                        </div>
                        <div class='clearfix'></div>"
                        """
                    lng: prof.geopos.coordinates[0]
                    lat: prof.geopos.coordinates[1]
                    icon: icon
                    group: "users"
                    layer: "users"



    return
]

