from django.contrib.auth.models import User
from map.models import UserProfile, Event
from rest_framework import routers, serializers, viewsets
from django.contrib.gis.db.models.functions import Distance

# custom thumbnail field
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from rest_framework_gis.serializers import GeometryField

class ThumbnailField(serializers.ImageField):
    def to_representation(self, instance):
        url = thumbnail_url(instance, 'avatar')
        if self.context["request"] and url:
            return self.context["request"].build_absolute_uri(url)
        else:
            return url

# Serializers define the API representation.
class SimpleUserSerializer(serializers.ModelSerializer):
    current = serializers.SerializerMethodField()
    def get_current(self, obj):
        if "request" in self.context and self.context["request"].user == obj:
            return True

        return False
    class Meta:
        model = User
        fields = ["id", "username", "current"]

class ProfileSerializer(serializers.ModelSerializer):
    user = SimpleUserSerializer()
    thumbnail = ThumbnailField(source="avatar")

    distance = serializers.SerializerMethodField()
    def get_distance(self, obj):
        if hasattr(obj, "distance"):
            return obj.distance.km

    class Meta:
        model = UserProfile
        fields = ("id", "user", "display_name", "description", "avatar", "thumbnail", "city", "state", "country", "geopos", "distance")

class EventSerializer(serializers.ModelSerializer):
    user = SimpleUserSerializer()
    thumbnail = ThumbnailField(source="logo")

    distance = serializers.SerializerMethodField()
    def get_distance(self, obj):
        if hasattr(obj, "distance"):
            return obj.distance.km

    class Meta:
        model = Event
        fields = ("id", "user", "title", "description", "begin_date", "end_date", "logo", "thumbnail",  "address", "zipcode", "city", "state", "country", "geopos", "distance")


# ViewSets define the view behavior.
class ProfileViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ProfileSerializer

    def get_queryset(self):
        queryset = UserProfile.objects.all().select_related("user")
        if self.request.user.is_authenticated():
            try:
                pos = self.request.user.userprofile.geopos
            except UserProfile.DoesNotExist:
                pass
            else:
                if pos:
                    queryset = queryset.annotate(distance=Distance('geopos', pos))
        return queryset


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Event.objects.all().select_related("user")
    serializer_class = EventSerializer

    def get_queryset(self):
        queryset = Event.objects.all().select_related("user")
        if self.request.user.is_authenticated():
            try:
                pos = self.request.user.userprofile.geopos
            except UserProfile.DoesNotExist:
                pass
            else:
                if pos:
                    queryset = queryset.annotate(distance=Distance('geopos', pos))
        return queryset


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'profiles', ProfileViewSet, base_name="userprofile")
router.register(r'events', EventViewSet)
