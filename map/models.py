from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.conf import settings
from django_countries.fields import CountryField
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Event(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, help_text=_("Tho user who is responsible for the event"))
    logo = models.ImageField(null=True, blank=True, upload_to="event_logos", help_text=_("A logo to display on the event page"))
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, help_text=_("Short description of the event"))
    begin_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True, help_text=_("Can be left empty"))

    # location specific fields
    geopos = models.PointField(verbose_name=_("Geo Location"))
    address = models.TextField(blank=True)
    zipcode = models.CharField(max_length=15, blank=True, verbose_name=_("ZIP Code"))
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100, blank=True)
    country = CountryField()

    class Meta:
        ordering = ("-begin_date", "title")

    def __unicode__(self):
        return self.title

class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    display_name = models.CharField(max_length=100)
    avatar = models.ImageField(null=True, blank=True, upload_to="user_avatars", help_text=_("Uploaded a photo that is displayed as an avatar next to your username"))
    description = models.TextField(blank=True, help_text=_("Short description of you and your unicycling activities"))
    phone = PhoneNumberField(blank=True)
    show_email = models.BooleanField(default=False, help_text=_("Show your email to other registered users"))

    # location specific fields
    geopos = models.PointField(verbose_name=_("Geo Location"), blank=True, null=True)
    city = models.CharField(max_length=100, blank=True)
    state = models.CharField(max_length=100, blank=True)
    country = CountryField(blank=True, null=True)


    def __unicode__(self):
        return self.display_name
