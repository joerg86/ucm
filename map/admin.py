from django.contrib.gis import admin
from django.contrib import admin as django_admin
from map.models import Event, UserProfile

class EventAdmin(admin.OSMGeoAdmin):
    list_display = ["id", "title", "begin_date", "end_date", "city", "country", "user"]
    date_hierarchy = "begin_date"
    list_filter = [
        ("user", django_admin.RelatedOnlyFieldListFilter), 
    ]
    search_fields = ["title", "city"]

class UserProfileAdmin(admin.OSMGeoAdmin):
    list_display = ["user", "display_name", "city", "country"]

# Register your models here.
admin.site.register(Event, EventAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
